[![pipeline status](https://gitlab.eclipse.org/eclipse/xfsc/tsa/cache/badges/main/pipeline.svg)](https://gitlab.eclipse.org/eclipse/xfsc/tsa/cache/-/commits/main)
[![coverage report](https://gitlab.eclipse.org/eclipse/xfsc/tsa/cache/badges/main/coverage.svg)](https://gitlab.eclipse.org/eclipse/xfsc/tsa/cache/-/commits/main)

# Cache service

Cache service exposes HTTP interface for working with Redis.

### Basic Architecture

```mermaid  
flowchart LR  
	A[Client] -- request --> B[HTTP API] 
	subgraph cache 
		B --> C[(Redis)] 
	end
```

### API Documentation

The API Documentation is accessible at `/swagger-ui` path in OAS 3.0 format. If you
use the docker-compose environment, it's exposed at `http://localhost:8083/swagger-ui`

### Dependencies

There must be a running instance of [Redis](https://redis.io/) visible to the service.
The address, username and password of Redis must be provided as environment variables.

Example:
```
REDIS_ADDR="localhost:6379"
REDIS_USER="user"
REDIS_PASS="pass"
```

### Development

This service uses [Goa framework](https://goa.design/) v3 as a backbone. 
[This](https://goa.design/learn/getting-started/) is a good starting point for learning to use the framework.

### Dependencies and Vendor

The project uses Go modules for managing dependencies, and we commit the `vendor` directory.
When you add/change dependencies, be sure to clean and update the `vendor` directory before
submitting your Merge Request for review.
```shell
go mod tidy
go mod vendor
```

### Tests and Linters

To execute the units tests for the service go to the root project directory and run:
```go
go test -race ./...
```

To run the linters go to the root project directory and run:
```go
golangci-lint run
```

## GDPR

[GDPR](GDPR.md)

## Dependencies

[Dependencies](go.mod)

## License

[Apache 2.0 license](LICENSE)
